#include <MIDI.h>
#include <SoftwareSerial.h>

#define BUFLEN 128
#define SHIFT 4 // SHIFT < BUFLEN
#define MEAN 4 // MEAN < BUFLEN

// #define DEBUG

#ifndef DEBUG
MIDI_CREATE_DEFAULT_INSTANCE();
#endif

int val_0 = -1;
int val_1 = -1;
int val_2 = -1;

int buf[BUFLEN];
int bufs[BUFLEN];
int bufpos = 0;

void setup() {
  pinMode(A1, INPUT);
#ifndef DEBUG
  MIDI.begin(MIDI_CHANNEL_OMNI);
#else
  Serial.begin(9600);
#endif
}

int partition(int tablica[], int p, int r) {// dzielimy tablice na dwie czesci, w pierwszej wszystkie liczby sa mniejsze badz rowne x, w drugiej wieksze lub rowne od x
  int x = tablica[p]; // obieramy x
  int i = p, j = r, w; // i, j - indeksy w tabeli
  while (true) { // petla nieskonczona - wychodzimy z niej tylko przez return j
    while (tablica[j] > x) // dopoki elementy sa wieksze od x
      j--;
    while (tablica[i] < x) // dopoki elementy sa mniejsze od x
      i++;
    if (i < j) {// zamieniamy miejscami gdy i < j
      w = tablica[i];
      tablica[i] = tablica[j];
      tablica[j] = w;
      i++;
      j--;
  } else // gdy i >= j zwracamy j jako punkt podzialu tablicy
    return j;
  }
}
 
void quicksort(int tablica[], int p, int r) { // sortowanie szybkie
  int q;
  if (p < r) {  
    q = partition(tablica,p,r); // dzielimy tablice na dwie czesci; q oznacza punkt podzialu
    quicksort(tablica, p, q); // wywolujemy rekurencyjnie quicksort dla pierwszej czesci tablicy
    quicksort(tablica, q+1, r); // wywolujemy rekurencyjnie quicksort dla drugiej czesci tablicy
  }
}

/* m audio settings -> raw value to 0-127 range */
float f_m(float x) {
  return -0.000205*x*x + 0.05*x + 124.2;
}

/* main program loop */
void loop() {

  /* fill begining of buffer with new samples */
  bufpos = 0;
  while (bufpos < SHIFT) {
    buf[bufpos] = analogRead(A1);
    bufpos++;
  }

  /* copy buffer to sorted table */
  for (int i = 0; i < BUFLEN; i++) {
    bufs[i] = buf[i];
  }
  
  /* sort table */
  quicksort(bufs, 0, BUFLEN-1);

  /* get mean value from middle */
  int acc = 0;
  for (int i = (BUFLEN-MEAN)/2; i < (BUFLEN+MEAN)/2; i++) {
    acc += bufs[i];
  }
  double valm = acc / (float) MEAN;

  /* custom curve */
  double yf = f_m(valm);

  /* detect change in value */
  int val = (int) yf;
  yf = (yf > 127) ? 127 : (yf < 0 ? 0 : yf);
  boolean change = val != val_0 & val != val_1 & val != val_2;
  if (val == -1 | change) {
    val_2 = val_1;
    val_1 = val_0;
    val_0 = val;
#ifdef DEBUG
      Serial.println(val);
#else
      MIDI.sendControlChange(70, val_0, 1);
#endif
  }
  
  /* shift table to make space for new samples */
  for (int i = BUFLEN-1; i >= SHIFT ; i--) {
    buf[i] = buf[i-SHIFT]; 
  }
}
